import { LitElement, html } from 'lit-element';

class TransportSidebar extends LitElement {

    static get properties(){
        return {
            peopleStats: {type: Object}

        };

    }

    constructor(){
        super();
        this.peopleStats ={} ;
    }
    render(){
        return html`

          <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
 
          <aside>
              <section>
                      <div class="mt-5">
                          <button @click="${this.newPoint}" class="btn btn-outline-success btn-lg">Alta Coche</button>
                      </div>
              </section>
          </aside>
          `;
            
         
    }

    newPoint(e){
        console.log("newPoint en persona-sidebar");
        console.log("Se va a crear un nuevo point");
        this.dispatchEvent(new CustomEvent("new-point",{}));
       

    }
}

customElements.define('transport-sidebar', TransportSidebar)

//<div>
//Hay <spam class"badge badge-pill badge-primary">  ${this.peopleStats.numberOffPeople}</spam>  personas
//</div>