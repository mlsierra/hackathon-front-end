import { LitElement, html } from 'lit-element';

class TransportFichaListado extends LitElement {

    static get properties(){
        return {
         
           idVehicle: {type: Number} ,
           freeSeats:{type: Number} ,
           plate:{type: String} ,
           model:{type: String},
           colour:{type: String},
           origin:{type: String},
           destination:{type: String},
           passengers:{
               name:{type: Array},
               phone:{type: Array},
               idClient:{type: Array}
           },
           disponible:{type: Boolean}
             
        };

    }

    constructor(){
        super();
        
    }
    render(){
        return html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
        <div class="card h-100">
           <div class="card-body">
              <h5 class="card-title"> Matricula: ${this.plate}</h5>
              <p class="card-text"> Origen:  ${this.origin}</p>
              <p class="card-text"> Destino: ${this.destination}</p>
   
              
          </div>
          <div class="card-footer">
          
                          
             <button @click="${this.deleteTransport}" class="btn btn-primary btn-lg btn-block">Eliminar</button>
             <button @click="${this.moreInfo}"     class="btn btn-secondary btn-lg btn-block">Modificar</button>
            

           </div>
        </div>

        

          `;
    }
    deleteTransport(e) {
        console.log("deleteTransport en transporta-ficha-listado");
        console.log("Se va a borrar el transporte " + this.idVehicle); 
        this.dispatchEvent(
            new CustomEvent("delete-transport", {
                    detail: {
                        idVehicle: this.idVehicle
                    }
                }
            )
        );
    }
    moreInfo(e){
        console.log("moreInfo");
        console.log("Se ha pedido la información del transport" +  this.idVehicle);

        this.dispatchEvent(
            new CustomEvent("info-transport",{
                detail:{
                    idVehicle:  this.idVehicle

                }

            })
        );
    }
          

}

customElements.define('transport-ficha-listado', TransportFichaListado)