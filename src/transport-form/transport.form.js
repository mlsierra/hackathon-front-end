import { LitElement, html } from 'lit-element';

class TransportForm extends LitElement {
    static get properties() {  
        return {              
            transport: {type: Object},
            editingTransport: {type: Boolean},   
            editingOrigin: {type: Boolean} ,  
            editingPoint: {type: Boolean},
            bajaPoint: {type: Boolean},
            idClient: {type: String},
            name: {type: String},
            phone : {type: String},
            idVehicle : {type: String}
 

        };  
    }  
    constructor() {         
        super();          
        this.resetFormData(); 
        console.log("editing point" + this.editingPoint);
        console.log('bajaPoint' + this.bajajPoint);
        this.editingPoint= false;
        this.bajaPoint=false;
        
        
    }
    updated(changedProperties) {          
        console.log("updated en transport-form");          
        if (changedProperties.has("editingPoint")) {             
            console.log("Ha cambiado el valor de la propiedad editingPoint en transport-form");             
            if (this.editingPoint === true) {                 
                this.shadowRoot.getElementById("transportPoint").classList.remove("d-none");          
            } else {                 
                this.shadowRoot.getElementById("transportPoint").classList.add("d-none");            
            }         
        }  
        console.log('bajaPoint' + this.bajaPoint);
        if (changedProperties.has("bajaPoint")) {             
            console.log("Ha cambiado el valor de la propiedad deletePoint en transport-form");  
            console.log('bajaPoint' + this.bajaPoint);           
            if (this.bajaPoint == true) {                 
                this.shadowRoot.getElementById("deletePoint").classList.remove("d-none");  
                console.log("se activa deletepoint ") ;       
            } else {                 
                this.shadowRoot.getElementById("deletePoint").classList.add("d-none");    
                console.log("se deactiva deletepoint ") ;            
            }         
        } 

    }
    
    render(){
        return html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
        <div>
            <form>
              <div class="form-group">
                 <label>Identificador</label>
                 <input type="text" @input="${this.updateIdVehicle}"  .value="${this.transport.idVehicle}"  ?disabled="${this.editingTransport}"  class="form-control" placeholder="Matricula" />
               </div>
               <div class="form-group">
                  <label>Matricula</label>
                  <input type="text" @input="${this.updatePlate}"  .value="${this.transport.plate}"   class="form-control" placeholder="Matricula" />
               </div>
               <div class="form-group">
                 <label>Modelo</label>
                 <input type="text" @input="${this.updateModel}"  .value="${this.transport.model}"   class="form-control" placeholder="Modelo" />
               </div>
               <div class="form-group">
                 <label>Color</label>
                 <input type="text" @input="${this.updateColour}"  .value="${this.transport.colour}"   class="form-control" placeholder="Color" />
               </div>
               <div class="form-group">
                 <label>Origen</label>
                 <input type="text" @input="${this.updateOrigin}"  .value="${this.transport.origin}"   ?disabled="${this.editingOrigin}" class="form-control" placeholder="Origen" />
               </div>
               <div class="form-group">
                 <label>Destino</label>
                 <input type="text" @input="${this.updateDestination}"  .value="${this.transport.destination}" ?disabled="${this.editingOrigin}"   class="form-control" placeholder="Destino" />
               </div>
               <div class="form-group">
                 <label>FreeSeats</label>
                 <input type="text" @input="${this.updateFreeSeats}"  .value="${this.transport.freeSeats}"  ?disabled="${this.editingTransport}"  class="form-control" placeholder="Plazas libres" />
               </div>
               <p class="card-text"><strong>Pasajeros apuntados: </strong></p>
               <div class="form-group"">
                     ${this.transport.passengers.map(
                                        passenger => html`
                                        <p class="card-text" .value="${passenger.idClient}"> Identif : ${passenger.idClient}</p>
                                        <p class="card-text" .value="${passenger.name}"> Nombre : ${passenger.name}</p>
                                        <p class="card-text" .value="${passenger.phone}" > Tefefono : ${passenger.phone}</p>
                                         `
                      )}
                     
                   </div>
                  <div class="form-group"" id="transportPoint">
                     <div class="form-group" >
                      <label>Cliente</label>
                      <input type="text" @input="${this.updateIdClient}"  ?disabled="${!this.editingPoint}"   class="form-control" placeholder="IdCliente" />
                     </div>
                     <div class="form-group">
                      <label>Nombre</label>
                       <input type="text" @input="${this.updateName}"  " ?disabled="${!this.editingPoint}"   class="form-control" placeholder="Nombre" />
                      </div>
                      <div class="form-group">
                      <label>Telefono</label>
                       <input type="text" @input="${this.updatePhone}"  " ?disabled="${!this.editingPoint}"   class="form-control" placeholder="Telefono" />
                      </div>
                    </div>
                    <div class="form-group" id="deletePoint">
                      <label>Cliente</label>
                      <input type="text" @input="${this.updateIdClient}"  ?disabled="${!this.bajaPoint}"   class="form-control" placeholder="IdCliente" />
                     </div>
  
                             

               <button @click="${this.goBack}" class="btn btn-outline-primary btn-lg">Atrás</button>
               <button @click="${this.storeTransport}" class="btn btn-outline-success btn-lg">Guardar</button>
               <button @click="${this.storePoint}"   class="btn btn-outline-success btn-lg"  ?disabled="${!this.transport.disponible}" ><strong>Apuntarse </strong></button>
               <button @click="${this.borrarPoint}"  class ="btn btn-outline-danger btn-lq">X</button>





            </form>
        </div>
          `;
    }
    goBack(e){
        console.log("goBack");
        //previene de hacer un submit en un boton dentro de un formulario
        e.preventDefault();
        this.resetFormData();
        this.dispatchEvent(new CustomEvent("transport-form-close", {}));
    }
    updateIdVehicle(e) {         
        console.log("uupdateIdVehicle");         
        console.log("Actualizando la propiedad idvehicle con el valor " + e.target.value);         
        this.transport.idVehicle = e.target.value; 
    }
    updatePlate(e) {         
        console.log("updatePlate");         
        console.log("Actualizando la propiedad matricula con el valor " + e.target.value);         
        this.transport.plate = e.target.value;     
    }     
    updateModel(e) {             
        console.log("updateModel");             
        console.log("Actualizando la propiedad modelo con el valor " + e.target.value);             
        this.transport.model = e.target.value;    
    }     
    updateColour(e) {             
         console.log("updateColour");             
         console.log("Actualizando la propiedad color con el valor " + e.target.value);            
         this.transport.colour = e.target.value;  
    }  

    updateOrigin(e) {             
        console.log("updateOrigin");             
        console.log("Actualizando la propiedad origen con el valor " + e.target.value);            
        this.transport.origin = e.target.value;  
    }  

    updateDestination(e) {             
       console.log("updateDestination");             
       console.log("Actualizando la propiedad destino con el valor " + e.target.value);   
               
       this.transport.destination = e.target.value;  
    }  
    updateFreeSeats(e) {             
        console.log("updateFreeSeats");             
        console.log("Actualizando la propiedad plazas libres con el valor " + e.target.value);            
        this.transport.freeSeats = e.target.value;  
     }  
     updateIdClient(e) {             
        console.log("updateIdClient");             
        console.log("Actualizando la propiedad cliente con el valor " + e.target.value); 
        this.idClient= e.target.value; 
    
        

     }  
     updateName(e) {             
        console.log("updateName");             
        console.log("Actualizando la propiedad nombre con el valor " + e.target.value); 
        this.name   =  e.target.value; 

     }  
     updatePhone(e) {             
        console.log("updatePhone");             
        console.log("Actualizando la propiedad telefono con el valor " + e.target.value); 
        this.idVehicle  =0;
        this.phone  = e.target.value; 
        console.log("phone " + this.phone);

     }  
    storeTransport(e) {      
        console.log("storeTransport");      
        e.preventDefault();        
        
        console.log("La propiedad plate vale " + this.transport.plate);      
        console.log("La propiedad model vale " + this.transport.model);      
        console.log("La propiedad cplour vale " + this.transport.colour);
        console.log("La propiedad origin  vale " + this.transport.origin);
        console.log("La propiedad destinatio vale " + this.transport.destination);    
              
        this.dispatchEvent(new CustomEvent("transport-form-store",{       
            detail: {        
                transport:  {      
                    idVehicle: this.transport.idVehicle,
                    freeSeats:this.transport.freeSeats,
                    plate:this.transport.plate,
                    model:this.transport.model,
                    colour:this.transport.colour,
                    origin:this.transport.origin,
                    destination:this.transport.destination,
                    passengers:this.transport.passengers,
                    disponible:this.transport.disponible
                    

                } ,  
                editingTransport: this.editingTransport,
                editingOrigin: this.editingOrigin
                
            }       
        })      
        ); 
        this.shadowRoot.getElementById("transportPoint").classList.add("d-none");  
        this.resetFormData();
 
    }
    storePoint(e){
        console.log("storePoint");      
        e.preventDefault();  
        console.log("editingPoint " + this.editingPoint)
        if (this.editingPoint === false){
             this.editingPoint = true
             
        }else{
            console.log("Gabar pasajero");
            this.transport.freeSeats = this.transport.freeSeats - 1;
            if (this.transport.freeSeats === 0 ){
                this.transport.disponible= false;
            }
            console.log(this.transport);
            console.log("passenger " + this.passenger);
            let newPassenger = {};
            newPassenger.idClient = this.idClient;
            newPassenger.name = this.name;
            newPassenger.idVehicle = this.idVehicle;
            newPassenger.phone = this.phone;

             console.log(newPassenger);
            this.dispatchEvent(new CustomEvent("transport-form-point",{       
                detail: {        
                    transport:  {      
                        idVehicle: this.transport.idVehicle,
                        freeSeats:this.transport.freeSeats,
                        plate:this.transport.plate,
                        model:this.transport.model,
                        colour:this.transport.colour,
                        origin:this.transport.origin,
                        destination:this.transport.destination,
                        passengers:this.transport.passengers,
                        disponible:this.transport.disponible
                       
                    },
                     newpassenger: newPassenger

                }       
            })      
            ); 
            this.resetFormData();
     
        }
             
     
    }

    borrarPoint(e) {
        console.log("borrarPoint en transport-form");
        e.preventDefault(); 
        console.log('bajaPoint' + this.bajaPoint); 
        if (this.bajaPoint == false){
            this.bajaPoint = true
            
       }else{
            console.log("Se va a borrar el pasajero " + this.idClient); 
              console.log(this.transport.idVehicle); 
           this.transport.freeSeats = this.transport.freeSeats + 1
           this.transport.disponible= true;

          this.dispatchEvent(
             new CustomEvent("delete-point", {
                    detail: {
                        transport: this.transport,
                        idVehicle : this.transport.idVehicle,
                        idClient: this.idClient
                    }
                }
             )
          );
          this.resetFormData();
        }
    }

    resetFormData(){
        console.log("resetFormData");
        this.transport = {};  
        this.transport.idVehicle=0; 
        this.transport.freeSeats=0;
        this.transport.plate = "";
        this.transport.colour = "";
        this.transport.model = "";  
        this.transport.origin = "";  
        this.transport.destination = ""; 
        this.transport.passengers= [] ;
        this.editingTransport = false;
        this.editingOrigin = false;

              
    }
}

customElements.define('transport-form', TransportForm)