import { LitElement, html } from 'lit-element';

import '../transport-header/transport-header.js';
import '../transport-main/transport-main.js';
import '../persona-main/persona-main.js';
import '../transport-footer/transport-footer.js';
import '../persona-alta/persona-alta.js';
import '../transport-sidebar/transport-sidebar.js';
//import '../persona-stats/persona-stats.js';

class TransportApp extends LitElement {

    static get properties(){
        return {
            showClientes: {type: Boolean}
           };
    }

    constructor(){
        super();
        
        this.showClientes=true;
    }
    
    updated(changedProperties) {          
        console.log("updated properties en transport-app");          
        if(changedProperties.has("showClientes")) {             
            console.log("Ha cambiado el valor de la propiedad showClientes en transport-app");             
            if (this.showClientes === true) {                 
                this.showClients();             
            } else {                 
                this.showPoint();             
            }         
        }  

    }

    showPoint() {         
        console.log("showpoint");         
        console.log("Mostrando Point");         
        this.shadowRoot.getElementById("point").classList.remove("d-none");            
        this.shadowRoot.getElementById("clients").classList.add("d-none");      
    } 

    showClients(){
        console.log("showClients");
        console.log("Mostrando el listado de clients");
        this.shadowRoot.getElementById("clients").classList.remove("d-none");
        this.shadowRoot.getElementById("point").classList.add("d-none");
    }

    render(){
        return html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
       <div class="container" >
        <transport-header
           @mostrar-clients="${this.mostrarClients}"
           @mostrar-point="${this.mostrarPoint}"    >
        </transport-header>
           <div id="clients">
             <div class="col-2"  >
              <persona-Alta @new-person="${this.newPerson}" class="col-1" ></persona-Alta>
             </div>

             <div>
              <persona-main class="col-11"></persona-main>
             </div>
          </div>


           
      

          
            <div  id="point">     
               <transport-sidebar @new-point="${this.newPoint}" class="col-1"></transport-sidebar>   
               <transport-main    class="col-11"></transport-main>
            </div>
        <transport-footer></transport-footer>
        
       </div>
          `;
    }
    mostrarPoint(e){
        console.log("mostrarPoint en transport-app");
        console.log("e.detail.showClientes " + e.detail.showClientes);
        console.log("this.showClientes " + this.showClientes) ;
        this.showClientes=e.detail.showClientes;
    }
    mostrarClients(e){
        console.log("mostrarClients en transport-app");
        console.log("e.detail.showClientes " + e.detail.showClientes);
        console.log("this.showClientes " + this.showClientes) ;
        this.showClientes=e.detail.showClientes;
    }


    
    newPoint(e) {         
        console.log("newPoint en transport-App");          
        this.shadowRoot.querySelector("transport-main").showTransportForm = true;     
    }   
    newPerson(e) {
        console.log("newPerson en transport-App");	
        this.shadowRoot.querySelector("persona-main").showPersonForm = true;
        console.log(this.shadowRoot.querySelector("persona-main").showPersonForm);	
    }
    /*
    peopleStatsUpdated(e){
        console.log("peopleStatsUpdated in pesona-app");

    }


    peopleStatsUpdated(e){
       console.log("peopleStatsUpdated");
       this.shadowRoot.querySelector("persona-sidebar").peopleStats = e.detail.peopleStats;

    }
    updatePeople(e){
        console.log("updatePeople");
        this.people = e.detail.people; 
    }

    updated(changedProperties){
        console.log("updated in pesona-app");
        if (changedProperties.has("people")) {
            console.log("ha cambiado people en persona-app");
            this.shadowRoot.querySelector("persona-stats").people = this.people;
        }
    }

    newPerson(e) {         
        console.log("newPerson en PersonaApp");          
        this.shadowRoot.querySelector("persona-main").showPersonForm = true;     
    }   
    */
}



customElements.define('transport-app', TransportApp)