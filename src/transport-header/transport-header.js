import { LitElement, html } from 'lit-element';


import 'https://unpkg.com/@material/mwc-button?module';

class TransportHeader extends LitElement {

    static get properties(){
        return {

        };

    }

    constructor(){
        super();

    }
    
    render(){
        return html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
         <div class="container">
         <div class="card-footer">
            <h1>App Transporte BBVA en Coche</h1>
            <button @click="${this.mostrarClients}" class="btn btn btn-primary btn-lg btn-block">Clientes</button>
            <button @click="${this.mostrarPoint}"   class="btn btn-secondary btn-lg btn-block">Apuntarse a un Coche</button>
            </div>
          </div> 

            
          `;
    }
    mostrarPoint(e){
        console.log("mostrarPoint");
        //this.shadowRoot.getElementById("transport-app").showClients= true;
        this.showClientes = false;
        this.dispatchEvent(
            new CustomEvent("mostrar-point",{
                  detail:{
                    showClientes : this.showClientes
                  }

            }));

    }
    mostrarClients(e){
        console.log("mostrarClients");
        //this.shadowRoot.getElementById("transport-app").showClients= true;
        this.showClientes = true;
        this.dispatchEvent(
            new CustomEvent("mostrar-clients",{
                detail:{
                    showClientes : this.showClientes
                }

            })
        
        );
}
}

customElements.define('transport-header', TransportHeader)