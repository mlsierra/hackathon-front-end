import { LitElement, html } from 'lit-element';

class PersonaFichaListado extends LitElement {

    static get properties() {
        return {
            fname: {type: String},
            idVehicle: {type: String},
            Phone: {type: String}, 
            idClient:{type: String}

        };
    }

    constructor() {
        super();
    }

    render() {
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
            <div class="container">
                <div class="card h-100">

                    <div class="card-body">
                    <h5 class="card-title">${this.fname}</h5>    
            

                        <label >idClient:<span class="red">  ${this.idClient}</span></label>
                    
                        
                        <label >Teléfono:<span class="red">  ${this.Phone}</span></label>
                    
        
                        <label >idVehicle:<span class="red">  ${this.idVehicle}</span></label>
        

                    </div>

                    
                    <div class="card-footer">
                            <button @click="${this.deletePerson}" class="btn btn-primary btn-lg btn-block">Eliminar</button>
                            <button @click="${this.moreInfo}"     class="btn btn-secondary btn-lg btn-block">Modificar</button>
                    </div>
                </div>     
            </div>          
        `;
    }

    moreInfo(e) {

        console.log("more ionfo")
        console.log("se ha pedido mas informacion de la persona " + this.fname);

        this.dispatchEvent(
                new CustomEvent("info-person", {
                    detail: {
                        name: this.fname
                    }

                }
            )
        );

    }

    deletePerson(e) {

        console.log("deletePerson en persona-main");
        console.log("se va a borrar la persona de nombre " +this.fname);

        this.dispatchEvent(
            new CustomEvent("delete-person", {
                            detail: {
                                    name: this.fname
                            }
                    }
            )
        );
        console.log("después de CustomEvent " +this.fname);

    }
}

customElements.define('persona-ficha-listado', PersonaFichaListado)