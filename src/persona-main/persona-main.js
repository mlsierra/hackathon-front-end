import { LitElement, html } from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado.js';
import '../persona-form/persona-form.js';

class PersonaMain extends LitElement {
    
    static get properties() {
        return {
            people: {type: Array},
            showPersonForm: {type: Boolean}
        };
    }

    constructor() {
        super();

       
        this.people = [
            {
                name: "Lila Bartolomeo",
                idVehicle: "1",
                Phone: "661652222",
                idClient: "1"
            }, {
                name: "Delphinia Frearson",
                idVehicle: "5", 
                Phone: "659852963",
                idClient: "2"
            }, {
                name: "Juan Bartolomeo",
                idVehicle: "1", 
                Phone: "666333444",
                idClient: "3"
            }, {
                name: "Juana Frearson",
                idVehicle: "5", 
                Phone: "654987456",
                idClient: "4"
            }, {
                name: "Alfredo Frearson",
                idVehicle: "6", 
                Phone: "632147896",
                idClient: "5"
            }, {
                name: "Juan Sin Miedo",
                idVehicle: "5", 
                Phone: "958563258",
                idClient: "11"
            }, {
                name: "Juana de Arco",
                idVehicle: "59", 
                Phone: "666999888",
                idClient: "70"
            }






        ];

        this.shownPersonForm = false;
    }



    updated(changedProperties) { 
        console.log("updated");	
        console.log(this.shownPersonForm);
        if (changedProperties.has("showPersonForm")) {
            console.log("Ha cambiado el valor de la propiedad showPersonForm en persona-main");
            if (this.showPersonForm === true) {
                this.showPersonFormData();
            } else {
                this.showPersonList();
            }
        }
    }


    showPersonList() {
        console.log("showPersonList");
        console.log("Mostrando listado de personas");
        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
        this.shadowRoot.getElementById("personForm").classList.add("d-none");	
    }  
    showPersonFormData() {
        console.log("showPersonFormData");
        console.log("Mostrando formulario de persona");
        this.shadowRoot.getElementById("personForm").classList.remove("d-none");	  
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");	
    } 


    render() {
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
            <h2 class="text-center"> Listado de Clientes </h2>
            <div class="row"  class="text-center" id="peopleList">
                <div class="row row-cols-2 row-cols-sm-6">
                    ${this.people.map(
                        person => html`<persona-ficha-listado 
                                            @delete-person="${this.deletePerson}"
                                            @info-person="${this.infoPerson}"
                                            fname="${person.name}" 
                                            idVehicle="${person.idVehicle}"
                                            Phone="${person.Phone}"
                                            idClient="${person.idClient}"
                                            >
                                    </persona-ficha-listado>`
                    )}
                </div>
            </div>

            <div class="row  row-cols-5  rol-cols-sm-6">

            <persona-form  
                @persona-form-store="${this.personFormStore}"
                @persona-form-close="${this.personFormClose}"
                id="personForm" 
                class="d-none border rounded border-primary">
            </persona-form>   


            </div>

        `;
    }


    

    deletePerson(e){
        console.log("deletePerson en persona-main");
        console.log("se va a borrar la persona de nombre " +e.detail.name); 

        console.log(this.people);

        this.people = this.people.filter(
                borrado => borrado.name != e.detail.name
                
        );

        console.log(this.people);
        console.log(this.people.filter);
        this.showPersonForm = false;
     
    }



    infoPerson(e) {
        console.log(" infoperson en persona main");
        console.log(" se ha pedido informacion de la persona " + e.detail.name);

        // filter devuelve un array
        let chosenPerson = this.people.filter(
            person => person.name === e.detail.name
        );

      //  console.log(chosenPerson);

      // trabajo con el cambio de propiedades. ponemos [0] por ser la primera posicion del array
        this.shadowRoot.getElementById("personForm").person = chosenPerson[0];
        this.shadowRoot.getElementById("personForm").editingPerson = true;
      // activimaos el formulario paraq ue se muestree.  
        this.showPersonForm = true;
    }

    
    personFormStore(e) {
        console.log("personFormStore  " ); 
        console.log("se va a almacenar una persona"); 

        console.log(e.detail.person); 
        
        if(e.detail.editingPerson === true)  {
            console.log("se va a actualizar una persona" + e.detail.person.name); 

            this.people = this.people.map(
                person => person.name === e.detail.person.name     
                   ? person = e.detail.person : person
            );


        } else {
            console.log("se va a almacenar una persona" + e.detail.person); 
          //  this.people.push(e.detail.person);

            // esto es igual al array enteroi con los tres puntos... es como descomponer el array
            //original y el ultimo elemento es el nuevo ahora si detecta el cambio del array

            this.people = [...this.people, e.detail.person];

            console.log("persona almacenada"); 

        }

        console.log("Proceso terminado"); 



        this.showPersonForm = false;
    }

    
    personFormClose(e) {
        console.log("personForm   Close");
        console.log("Se ha cerrado el formulario de la persona");
        this.showPersonForm = false;
    }

}

customElements.define('persona-main', PersonaMain)