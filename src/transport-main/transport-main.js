import { LitElement, html } from 'lit-element';

import '../transport-ficha-listado/transport-ficha-listado.js';
import '../transport-form/transport.form.js';

class TransportMain extends LitElement {
        
        
        static get properties(){
            return {
                transport: {type: Array},
                showTransportForm: {type: Boolean},
                idVehicleSig : {type: Number},
            };
    
        }
    
        constructor(){
               super();
               this.getVehicles();
              this.transport=[
                //vehiculo 1
                {"idVehicle":1,
                "freeSeats":0,
                "plate":"5213GG",
                "model":"Bentley",
                "colour":"Crimson",
                "origin":"Österbybruk",
                "destination":"Zafarwāl",
                "passengers":
                 //Passenger 1
                [{"idClient":1,
                "name":"Nev Gingell",
                "idVehicle":1,
                "phone":"915115307"},
                //Passenger 2
                {"idClient":2,
                "name":"Ettie Vasyanin",
                "idVehicle":2,
                "phone":"855945654"},
                //Passenger 3
                {"idClient":3,
                "name":"Lia Kohneke",
                "idVehicle":3,
                "phone":"844805649"},
                //Passenger 4
                {"idClient":4,
                "name":"Broddie Kollasch",
                "idVehicle":4,
                "phone":"462482435"}],
                "disponible":false},
                //vehiculo 2
               {"idVehicle":2,
               "freeSeats":0,
               "plate":"6241VK",
               "model":"Dodge",
               "colour":"Teal",
               "origin":"Palcamayo",
               "destination":"Mengzhai",
               "passengers":
               //Passenger 1
               [{"idClient":1,
               "name":"Murray Danels",
               "idVehicle":1,
               "phone":"260353746"},
               //Passenger 2
               {"idClient":2,
               "name":"Olin Gunson",
               "idVehicle":2,
               "phone":"322468710"},
               //Passenger 3
               {"idClient":3,
               "name":"Vidovic Rannie",
               "idVehicle":3,
               "phone":"908817927"},
               //Passenger 4
               {"idClient":4,
               "name":"Aron Abadam",
               "idVehicle":4,
               "phone":"239786331"},
               //Passenger 5
               {"idClient":5,
               "name":"Hyacinthe Remmer",
               "idVehicle":5,
               "phone":"208269827"}],
               "disponible":false},
                //vehiculo 3
               {"idVehicle":3,
               "freeSeats":1,
               "plate":"7116UQ",
               "model":"Mercedes-Benz",
               "colour":"Violet",
               "origin":"Omurtag",
               "destination":"Samdrup Jongkhar",
               "passengers":
               //Passenger 1
               [{"idClient":1,
               "name":"Liesa Odams",
               "idVehicle":1,
               "phone":"610667247"},
               //Passenger 2
                {"idClient":2,
               "name":"Ahmed Bohje",
               "idVehicle":2,
               "phone":"942444707"},
               //Passenger 3
               {"idClient":3,
               "name":"Lorens Valerius",
               "idVehicle":3,
               "phone":"516954719"},
               //Passenger 4
               {"idClient":4,
               "name":"Farley Nyssens",
               "idVehicle":4,
               "phone":"668372214"}],
               "disponible":true},
                 //vehiculo 4
               {"idVehicle":4,
               "freeSeats":1,
               "plate":"9674EL",
               "model":"Smart",
               "colour":"Aquamarine",
               "origin":"Marabá",
               "destination":"Huntington",
               "passengers":
               //Passenger 1
               [{"idClient":1,
               "name":"Christy Golland",
               "idVehicle":1,
               "phone":"614165441"},
               //Passenger 2
               {"idClient":2,
               "name":"Darby Fennelly",
               "idVehicle":2,
               "phone":"462548981"},
               //Passenger 3
               {"idClient":3,
               "name":"Dalila Howship",
               "idVehicle":3,
               "phone":"353983660"}],
               "disponible":true}
                ];
            
              
            
               this.showTransportForm=false;
               //this.idVehicleSig = 0; 
               //this.transport.map( transport => {
               //    if (transport.idVehicle > this.idVehicleSig ) { this.idVehicleSig = transport.idVehicle}
               //});
                //console.log(idVehicleSig);
        }

        getVehicles(){         
            console.log("getVehicles");         
            console.log("Obteniendo datos de la vehiculos");          
            let xhr = new XMLHttpRequest();         
            xhr.open("GET", "http://localhost:8080/apivehicles/v1/vehicles");         
            xhr.send();         
            console.log("Información enviada");                   
            xhr.onload=()=>{ 
                  if (xhr.status === 200) { 
                        console.log("Petición completada correctamente");                 
                        let APIResponse = JSON.parse(xhr.responseText);                 
                        this.movies = APIResponse.results;             
                    }          
                }     
        } 

        updated(changedProperties) {          
            console.log("updated en transport-main");          
            if (changedProperties.has("showTransportForm")) {             
                console.log("Ha cambiado el valor de la propiedad showtransportForm en transport-main");             
                if (this.showTransportForm === true) {                 
                    this.showTransportFormData();             
                } else {                 
                    this.showTransportList();             
                }         
            }  
            
      //      if(changedProperties.has("transport")){
      //          console.log("Ha cambiado la propiedad  transport en transport-main");
      //          this.dispatchEvent(
      //              new CustomEvent("update-transport", {
      //                  detail:{
      //                      transport : this.transport.idVehicle  
      //                  } 
      //              })
      //          );
      //      }
        }
    
        showTransportFormData() {         
            console.log("showTransportFormData");         
            console.log("Mostrando formulario de transporta");         
            this.shadowRoot.getElementById("transportForm").classList.remove("d-none");            
            this.shadowRoot.getElementById("transportList").classList.add("d-none");      
        } 
    
        showTransportList(){
            console.log("showTransportList");
            console.log("Mostrando el listado de transportes");
            this.shadowRoot.getElementById("transportList").classList.remove("d-none");
            this.shadowRoot.getElementById("transportForm").classList.add("d-none");
        }
        render(){
            return html`
              <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
              <h2 class="text-center">Listado de Coches</h2>
              <str>
              <div class="row" id="transportList"> 
                   <div class="row row-cols-2 row-cols-sm-4">
                     ${this.transport.map(
                                        transport => html`<transport-ficha-listado                                                       
                                                          idVehicle="${transport.idVehicle}" 
                                                          freeSeats="${transport.freeSeats}" 
                                                          plate="${transport.plate}" 
                                                          model="${transport.model}"
                                                          colour="${transport.colur}"
                                                          origin="${transport.origin}"
                                                          destination="${transport.destination}"
                                                          passengers="${transport.passengers}"
                                                          disponible="${transport.disponible}" 
                                                          @delete-transport="${this.deleteTransport}"
                                                          @info-transport="${this.infoTransport}"
                                                         
                                              >
                                             </transport-ficha-listado>`
                      )}
                     
                   </div>
                   
              </div>
              <div class= "row row-cols-1  row-cols-sm-2"  >
                <transport-form id="transportForm" class="d-none border rounded border-primary"                               
                  @transport-form-close="${this.transportFormClose}"                               
                  @transport-form-store="${this.transportFormStore}"      
                  @transport-form-point="${this.transportFormPoint}"
                  @delete-point="${this.deletePoint}">
                </transport-form>
                
              </div>
           
            `;
              
        }
        deleteTransport(e) { 
            console.log("deletetransport en transporta-main");
            console.log("Se va a borrar la transporta  " + e.detail.idVehicle);
            this.transport = this.transport.filter(
                transport => transport.idVehicle != e.detail.idVehicle
            );
    
        }

        deletePoint(e) { 
            console.log("deletePoint en transporta-main");
            console.log('vehiculo a borrar ' + e.detail.idVehicle);
            console.log("Se va a borrar el pasajero " + e.detail.idClient);
            console.log(this.transport);
             

            this.transport = this.transport.map(
                transport => transport.idVehicle === e.detail.idVehicle 
                ?  transport = e.detail.transport : transport 
            );

             let vehicle  = this.transport.filter(
               transport => transport.idVehicle == e.detail.idVehicle
            );
           console.log(vehicle[0]);

           vehicle[0].passengers = vehicle[0].passengers.filter(
               passenger => passenger.idClient != e.detail.idClient
           );
               
        }
    
        transportFormClose(e) {  
           this.showTransportForm=false;   
           console.log("transportFormClose");         
           console.log("Se ha cerrado el formulario de la transporta");         
        }     
 
        transportFormStore(e) {         
            console.log("transportFormStore");         
            console.log("Se va a almacenar un  vehiculo");  
            if (e.detail.editingTransport){
                
                console.log("transporta modificada:  " + e.detail.transport.idVehicle);
                /* let indexOFtransport = 
                    this.transport.findIndex(
                        transport => transport.idVehicle === e.detail.transport.idVehicle
                    );
                
                console.log("El indice a actualizar es: " + indexOFtransport);                
                if (indexOFtransport >= 0){
                    console.log("transporta actualizada");
                    this.transport[indexOFtransport] = e.detail.transport;
                }
                */
                
               this.transport = this.transport.map(
                    transport => transport.idVehicle === e.detail.transport.idVehicle 
                    ?  transport = e.detail.transport : transport 
                );
                
            } else {
                //this.transport.push(e.detail.transport);

                console.log(e.detail.transport);
                console.log(e.detail.transport.passengers);
                this.transport  = [...this.transport, e.detail.transport];
                console.log("transporte almacenado" + e.detail.transport.idVehicle);
            }                                   
            console.log("Proceso terminado ");               
                      
            this.showTransportForm = false;     
        }

        transportFormPoint(e) {         
            console.log("transportFormPoint");         
            console.log("Se va a almacenar un pasajero");  
            
            console.log(e.detail.transport.idVehicle); 

            
            let vehiculo =  this.transport.filter(
                transport => transport.idVehicle === e.detail.transport.idVehicle
            );
            
            console.log(vehiculo); 
            console.log(vehiculo[0]); 
            console.log(e.detail.newpassenger); 
            vehiculo[0].passengers.push(e.detail.newpassenger); 
   
                       

            this.showTransportForm = false; 
        }

        infoTransport(e){
            console.log("infortransport en transporta.main");
            console.log("Se ha pedido información de la transporta:" + e.detail.idVehicle)
            
            let choosentransport = this.transport.filter(
                transport => transport.idVehicle === e.detail.idVehicle
    
            );
            
            this.shadowRoot.getElementById("transportForm").transport= choosentransport[0];
            this.shadowRoot.getElementById("transportForm").editingTransport= true;

            if (this.shadowRoot.getElementById("transportForm").transport.passengers.length !== "undefined")  {
            let totpassengers = this.shadowRoot.getElementById("transportForm").transport.passengers;
            console.log(totpassengers);
            if (totpassengers.length < 0 ) {
                this.shadowRoot.getElementById("transportForm").editingOrigin= false;
              } else {
                this.shadowRoot.getElementById("transportForm").editingOrigin= true;
              }
            console.log("editingOrigin" + this.shadowRoot.getElementById("transportForm").editingOrigin ) 
            }
            this.showTransportForm=true;
        }
    
    }

customElements.define('transport-main', TransportMain)