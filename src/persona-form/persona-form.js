import {LitElement,html} from 'lit-element';

class PersonaForm extends LitElement {
        static get properties() {
                return{
                        person: {type: Object}
                  
                };
        }

        constructor() { 
                super();		
                this.person = {};
        }        


    render() {
        return html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
        
        <div class="container">
            <form >
            <div >
            Ficha de alta de cliente
            </div  class="container">

                <div class="form-group">
    

                        <label> Nombre Completo <label/>
                        <input type="text" @input="${this.updateName}" .value="${this.person.name}" id="personaFormName"  placeholder="Nombre Completo"/>
                 </div>


                <div class="form-group">
                        <label> Id Vehiculo <label/>
                        <input type="text" .value="${this.person.idVehicle}" disabled  placeholder="idVehicle" rows="1">
         
                        <label> Teléfono <label/>
                        <input type="text" @input="${this.updatePhone}" .value="${this.person.Phone}" placeholder="teléfono"/>

                        <label> idClient <label/>
                        <input type="text" disabled  .value="${this.person.idClient}"  placeholder="idClient"/>
                </div>

                <button @click="${this.goBack}" class="btn btn-outline-primary btn-lg">Atrás</button>
                <button @click="${this.storePerson}"class="btn btn-outline-success btn-lg">Guardar</button>


            </form>
        </div>

        `;

    }

    updateName(e) {
        console.log("updateName");
        console.log("Actualizando la propiedad name con el valor " + e.target.value);
        this.person.name = e.target.value;
        this.person.idClient = 9;
        this.person.idVehicle = 15;

    }

    updatePhone(e) {
		console.log("updateTelefono");
		console.log("Actualizando la propiedad updateTelefono con el valor " + e.target.value);
		this.person.Phone = e.target.value;
    }
    updateidClient(e) {
        console.log("updateidClient");
        console.log("Actualizando la propiedad updateTelefono con el valor " + e.target.value);
        this.person.idClient = e.target.value;
}

    goBack(e) {
            console.log("goBack");	  
            e.preventDefault();	
            this.dispatchEvent(new CustomEvent("persona-form-close",{}));	
    }

    storePerson(e) {
	console.log("storePerson");	  
        e.preventDefault();	
        
		
	console.log("La propiedad name vale " + this.person.name);
	console.log("La propiedad Telefono vale " + this.person.Phone);
	console.log("La propiedad idVehicle vale " + this.person.idVehicle);	
	console.log("La propiedad idClient vale " + this.person.idClient);	
			


	this.dispatchEvent(new CustomEvent("persona-form-store",{
                detail: {
			person:  {
					name: this.person.name,
					Phone: this.person.Phone,
					idVehicle: this.person.idVehicle,
					idClient: this.person.idClient
				}
			}
               })
        );	
    }

}

customElements.define('persona-form',PersonaForm)