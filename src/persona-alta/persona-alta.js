import {LitElement,html} from 'lit-element';

class PersonaAlta extends LitElement {
    render() {
        return html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
 
        <aside>
            <section>
                    <div class="mt-5">
                        <button @click="${this.newPerson}" class="btn btn-outline-success btn-lg">Alta Nueva</button>
                    </div>
            </section>
        </aside>
        `;
    }

    newPerson(e) {
        console.log("newPerson en persona-Alta");
        console.log("se va a crear una nueva persona");

        this.dispatchEvent(new CustomEvent("new-person", {}));
    }


}

customElements.define('persona-alta',PersonaAlta)